# Como funciona a pontuação

## Visão geral

A pontuação de engajamento de Perusall permite que você combine várias métricas de engajamento do aluno em uma única pontuação. Até seis componentes diferentes podem ser combinados em uma única pontuação para incentivar os alunos a se envolverem em comportamentos que a pesquisa mostra que podem prever resultados de aprendizagem positivos. Cada um dos seis componentes tem uma meta que pode ser definida de 0 a 100 e representa o crédito máximo que o aluno pode ganhar com esse componente. A pontuação do aluno em cada um dos seis alvos é somada; uma pontuação final de 100 ou acima será traduzida para a pontuação máxima possível da atribuição (que você pode alterar em Configurações> Avançado), e as pontuações finais entre 0 e 100 serão dimensionadas para o intervalo de pontuação da atribuição. Você pode definir as metas para que totalizem mais de 100, o que indica que os alunos podem ganhar o crédito total de várias maneiras. (Definir uma meta para 0 significa que o componente correspondente não será incorporado na pontuação de envolvimento.) Nós o encorajamos a fazer isso para que a barreira para crédito total não seja tão alta que os alunos coloquem um foco indevido na nota - nós gostaríamos muito mais que os alunos se concentrassem em se envolver em uma discussão natural! O objetivo da pontuação de Perusall não é diferenciar os alunos, mas sim motivá-los a participar; como resultado, acreditamos que errar por ser mais generoso é benéfico.

## Comunicar aos alunos sobre a pontuação

Sugerimos que forneça aos alunos orientações gerais sobre a pontuação, sem entrar nos detalhes das métricas que você selecionou. (Você pode personalizar uma mensagem de boas-vindas para os alunos em Configurações> Geral.) Acreditamos firmemente que definir com muita precisão como o nível de envolvimento dos alunos é avaliado envia a mensagem errada aos alunos e os incentiva a tentar "manipular" o algoritmo de avaliação. Isso leva à percepção do aluno de que as atribuições de leitura são apenas “trabalhos ocupados”; em vez disso, queremos que os alunos estejam intrinsecamente motivados a “se engajar” em Perusall porque eles veem a conexão entre as leituras e seu próprio domínio do material de leitura. Vemos a pontuação como uma forma de convidar os alunos a participarem da conversa, ao invés de um fim em si mesma.


## Liberando partituras

Cada tarefa é avaliada automaticamente conforme os alunos trabalham. As pontuações de cada tarefa aparecerão em seu diário de classe, mas não aparecerão para os alunos até que você libere as pontuações. Você pode liberar pontuações de três maneiras:
● Manualmente, no diário de classe, depois de revisar as pontuações dessa tarefa.
● Automaticamente, após o prazo final da tarefa, para que você não precise liberar pontuações manualmente após cada tarefa.
● Automaticamente, enquanto os alunos estão trabalhando. Geralmente não recomendamos essa opção, pois pode fazer com que os alunos tentem “manipular” o sistema em vez de participar totalmente das discussões.


## Componentes de pontuação

### Componente "qualidade de anotação"

Cada comentário ou pergunta no Perusall é pontuado automaticamente pelo sistema e é classificado como abaixo das expectativas, atende às expectativas ou excede as expectativas.
Nas configurações do seu curso, você decide quantos pontos cada uma dessas categorias deve valer (por padrão 0, 1 e 2 pontos, respectivamente).
Você também pode definir os seguintes parâmetros nas configurações do curso:

- Quantos comentários / perguntas de cada aluno serão levados em consideração ao calcular sua nota para a tarefa (por padrão, 7). O Perusall pontuará todos os comentários e perguntas dos alunos e usará apenas a pontuação média de suas anotações de melhor pontuação ao calcular sua pontuação.
- A penalidade máxima por não distribuir uniformemente as anotações ao longo do capítulo (por padrão, 10%).
- A escala de pontuação final (por padrão, 0-3). As pontuações finais dos alunos para cada tarefa são redimensionadas para esta escala e, em seguida, arredondadas para o número inteiro mais próximo.

Se você definir uma janela de resposta pós-prazo em suas configurações de pontuação, os alunos podem responder a comentários ou perguntas existentes para obter o crédito total por um determinado período de tempo após o prazo. (Isso é para incentivar os alunos a responder às perguntas e comentários uns dos outros, mesmo quando a pergunta ou comentário inicial for feito perto do prazo.) No entanto, observe que os alunos não podem ganhar mais crédito após o prazo ao inserir as respostas do que já ganharam seu trabalho antes do prazo. (Esta verificação existe para evitar que os alunos simplesmente iniciem a tarefa após o prazo e ganhem crédito total simplesmente respondendo às perguntas ou comentários de outros alunos.) Se você definir um período de anotação atrasado em suas configurações de pontuação, Perusall concederá automaticamente o crédito parcial para anotações feitas após o prazo com base em quando o comentário foi feito. Por exemplo, comentários feitos três quartos durante o período de anotação atrasado receberão 25% do crédito que teriam recebido se feitos dentro do prazo.

### Componente "abrir a tarefa"

Para incentivar os alunos a dividir seu trabalho em várias sessões (em vez de apenas fazer todas as leituras e comentários em uma única sessão), você pode dar aos alunos crédito por abrir a tarefa várias vezes. A configuração de incremento da tarefa de abertura indica qual fração da meta é obtida para cada vez que o aluno abre a tarefa.

### Componente de "leitura"

Para incentivar os alunos a lerem até o final da tarefa, os alunos ganharão uma porcentagem rateada da meta de leitura com base na porcentagem das páginas ou seções que abrirem. (Se você atribuir uma única página ou uma única seção de um livro que pode ser otimizado, o aluno receberá o crédito total por este componente automaticamente.)

### Componente de "tempo de leitura ativo"

Para incentivar os alunos a dedicarem tempo à leitura, você pode dar aos alunos crédito por cada minuto gasto lendo ativamente (ou seja, sem contar o tempo quando o Perusall está aberto, mas eles não estão interagindo ativamente com o navegador). A configuração de incremento de leitura ativa indica qual fração da meta é obtida para cada minuto gasto lendo ativamente.


### Componente de "obter respostas"

Para incentivar os alunos a fazerem perguntas e comentários ponderados que gerem uma boa discussão, você pode dar crédito aos alunos pelas respostas que você e outros alunos postam em seus comentários. A configuração de incremento de obtenção de respostas indica qual fração da meta é obtida para cada resposta postada em seus comentários.

### Componente de "upvoating"

Para incentivar os alunos a "votar positivamente" nas perguntas e comentários uns dos outros (ou seja, clique no ponto de interrogação para indicar "Eu tenho a mesma pergunta" ou clique na marca de seleção verde para indicar "esta resposta ajudou minha compreensão"), você pode dar crédito aos alunos quando eles votam positivamente nas anotações de outras pessoas, bem como quando escrevem boas perguntas ou comentários que são votados por outras pessoas. A configuração de incremento de votos positivos indica qual fração da meta é ganha quando alguém dá um voto positivo no comentário do aluno, e a configuração de incremento de votos positivos indica que fração da meta é ganha quando o aluno dá um voto positivo na pergunta ou comentário de outro aluno.

## Exemplo

Abaixo está um exemplo de como uma atribuição específica pode ser pontuada, com base nas metas observadas abaixo. Observe que os valores-alvo somam mais de 100%, o que significa apenas que o aluno pode ganhar o crédito integral de várias maneiras.

**Componente de qualidade de anotação (alvo de 60%)**

Suponha que uma tarefa específica exija 4 anotações para crédito total e um aluno envie 5 comentários, com pontuações de 0, 0, 1, 2 e 2. Perusall considerará apenas as 4 melhores anotações, portanto, apenas as pontuações 0, 1, 2 e 2 será usado para calcular a pontuação. A pontuação de qualidade média é (0 + 1 + 2 + 2) / 4 = 1,25. Agora a penalidade de distribuição é aplicada. Suponha que a pontuação de distribuição do aluno seja 4/5 e a penalidade de distribuição máxima possível seja 10% (o padrão de Perusall). Isso significa que a pontuação de qualidade média de 1,25 será reduzida em 1,25 * (1 - 4/5) * (10%) = 0,025, para 1,225, de um máximo de 2. Como a meta é 60, a pontuação do aluno em o componente de qualidade da anotação é (1,225 / 2) * 60 = 36,75.

**Componente de atribuição de abertura (meta de 20%)**

Suponha que o incremento da tarefa de abertura seja de 5% e o aluno tenha aberto a tarefa 3 vezes. Isso significa que a pontuação do aluno no componente da tarefa de abertura é (3 * 5%) * 20 = 3.

**Componente de leitura (meta de 20%)**

Suponha que a tarefa tenha 15 páginas e o aluno tenha lido 12 delas. Isso significa que a pontuação do aluno na leitura do componente material é (12/15) * 20 = 16.

**Componente de tempo de leitura ativo (meta de 10%)**

Suponha que o aumento do gasto de tempo lendo ativamente seja de 0,5%, e o aluno tenha lido ativamente por 20 minutos. Isso significa que a pontuação do aluno na leitura do componente material é (20 * 0,5%) * 10 = 1.

**Componente de obtenção de respostas (meta de 20%)**

Suponha que o incremento de obtenção de respostas seja de 1% e que o aluno tenha escrito perguntas e comentários que geraram um total de 30 respostas. Isso significa que a pontuação do aluno no componente de obtenção de respostas é (30 * 1%) * 20 = 6.

**Componente de votação positiva (meta de 20%)**

Suponha que o incremento de votos positivos seja de 1% e o incremento de votos positivos de recebimento seja de 2%. Suponha que das 5 anotações postadas do aluno, três não receberam votos positivos, um recebeu 4 votos positivos e um recebeu 3 votos positivos. E suponha que o aluno tenha votado positivamente em 10 outros comentários. Isso significa que a pontuação do aluno no componente de votação positiva é (7 * 2% + 10 * 1%) * 20 = 4,8

**Cálculo final**

Somando as pontuações de cada um dos seis componentes, obtém-se 36,75 + 3 + 16 + 1 + 6 + 4,8 = 67,55 de um total de 100. Isso é redimensionado para a escala de pontuação da atribuição final de 0-3 por meio do cálculo 67,55 / 100 * 3 = 2,0265. Ele é arredondado para o número inteiro mais próximo, de modo que o aluno receberia uma pontuação final de 2 na tarefa.


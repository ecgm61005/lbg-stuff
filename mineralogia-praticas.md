# PL1

## dureza e escala Mohs
**dureza**
- a dureza de um mineral é a resistência que a sua superfície oferece ao ser riscada
- o grau de dureza é determinado pela comparação da dificuldade ou facilidade com que um mineral é riscado
- principio: um material mais duro irá riscar/deixa um risco um material mais mole
- este principio de medição relativa já é usado desde a antiguidade. A escala usada em mineralogia é a de Mohs. Outras modificações á escala, como a de Ridgway, têm mais aplicação na industria

**escala Mohs de dureza de minerais**
- é uma escala qualitativa, relativa e ordinal (de 1 a 10)
- baixa precisão mas prática no campo
- criada em 1822 por Friedrich Mohs
- composta por minerais que são fácies de encontrar, á excepção do diamante
- ao talco atribui o valor 1 e ao diamante o 10
- não é linear

![nach moh harte skala](img/mohs.png)<br>
![moh scale wrong minerals](img/mohs-wrong.png)<br>
![moh scale ok](img/mohs-ok.jpg)<br>

Os minerais da escala:
1. Talco
2. Gesso

a unha tem uma dureza entre 2 e 3. É fácil arranhar o talco. O gesso já é mais complicado e a calcite impossivel de arranhar.

3. Calcite
4. Fluorite
5. Apatite

o canivete/tesoura de aço (liga de Fe e C) e o vidro estão aqui algures

6. Feldspato (ortoclásio/potássico)

a placa de porcelana

7. Quartzo
8. Topázio
9. Corindo
10. Diamante

Não há diamante no lab... Normalmente temos lá o íman.

![mohs minerals](img/mohs-minerals.png)


procedimento:<br>
"é riscar mesmo, não é fazer festinhas"<br>
"se não querem usar a mão, deixem crescer e cortem a unha do pé"<br>
temos 2 minerais, A e B<br>
Se o A risca o B, o A é mais duro<br>
Se o A não rsica o B, o B é mais duro<br>
Se A e B têm a mesma dureza, serão ineficientes na produção de riscos um no outro (no entanto, podem surgir pequenos riscos)


Extra:
- pode-se falar de dureza de uma forma mais geral, como sendo uma medida da resistência a deformação plástica localizada
- riscar (fricção com outro objecto), como na escala de Mohs, não é a única forma de induzir essa deformação plástica.<br>
Na [dureza á indentação](https://en.wikipedia.org/wiki/Indentation_hardness), a deformação é devida a uma carga compressiva constante. Há varias escalas, conforme a natureza do material que se quer avaliar (há para metais, cerâmicos, polímeros), geralmente absolutas.<br>
eg. [teste dureza Brinnell](https://www.youtube.com/watch?v=RJXJpeH78iU), [Vickers](https://www.youtube.com/watch?v=7Z90OZ7C2jI), [Rockwell](https://www.youtube.com/watch?v=G2JGNlIvNC4)<br>
Também aparece na net uma [dureza dinâmica/de resalto](https://material-properties.org/what-is-rebound-hardness-definition/) em que se mede a altura do ressalto de um objecto deixado cair de uma altura fixa... Nunca ouvi falar disto, não me parece que seja muito usado.
- supostamente o cobre tem dureza entre 3 e 4, podendo então arranhar a calcite. Por isso, uma referencia parece ser uma moeda de cobre. Mas não sei até que ponto isto é aplicável ás moedas de euro. Elas têm cobre mas... em concreto,  1, 2 e 5 são aço banhado a cobre e as de 10, 20 e 50 são ligas metálicas de cobre. Podem testar e ver.
- em ingles,<br>
duro = hard, em oposição a mole = soft<br>
dureza ao risco = scratch hardness<br>
à indentação = indentation hardness<br>
talco = talc/talcum
gesso/gipsita = gypsum

https://en.wikipedia.org/wiki/Mohs_scale_of_mineral_hardness



## formas simples

TODO!

# PL2

identificação de minerais em amostra de mão

O objectivo será decorar a seguinte 
## lista de minerais

**ELEMENTOS NATIVOS**
- [Grafite](http://rruff.info/doclib/hom/graphite.pdf)
- [Enxofre nativo](http://rruff.info/doclib/hom/sulphur.pdf)

**ÓXIDOS**
- [Corindo](http://rruff.info/doclib/hom/corundum.pdf)
- [Magnetite](http://rruff.info/doclib/hom/magnetite.pdf)
- [Hematite](http://rruff.info/doclib/hom/hematite.pdf)

**SILICATOS**
- Grupo das olivinas

    Exemplo: [forsterite](http://rruff.info/doclib/hom/forsterite.pdf)

- Grupo das granadas

    Exemplo: [andradite](http://rruff.info/doclib/hom/andradite.pdf)

- [Andaluzite](http://rruff.info/doclib/hom/andalusite.pdf)
- [Topázio](http://rruff.info/doclib/hom/topaz.pdf)
- [Estaurolite](http://rruff.info/doclib/hom/staurolite.pdf)

- Grupo das turmalinas
    
    Exemplo: [schorlite](http://rruff.info/doclib/hom/schorl.pdf)

- [Berilo](http://rruff.info/doclib/hom/beryl.pdf)
- Grupo das piroxenas
    
    [Espodumena](http://rruff.info/doclib/hom/spodumene.pdf)

- [Talco](http://rruff.info/doclib/hom/talc.pdf)
- Grupo das micas

    Exemplos:
    - [Moscovite](http://rruff.info/doclib/hom/muscovite.pdf)
    - [Biotites](http://rruff.info/doclib/hom/biotite.pdf)
    - [Lepidolite](http://rruff.info/doclib/hom/lepidolite.pdf)

- Grupo das clorites
    
    Exemplo: [clinocloro](http://rruff.info/doclib/hom/clinochlore.pdf)

- [Quartzo](http://rruff.info/doclib/hom/quartz.pdf)
- Grupo dos feldspatos
    
    Exemplo: [microclina](http://rruff.info/doclib/hom/microcline.pdf)

**FOSFATOS**
- Grupo das apatites
    
    Exemplo: [fluoroapatite](http://rruff.info/doclib/hom/apatitecaf.pdf)

**CARBONATOS**
- [Calcite](http://rruff.info/doclib/hom/calcite.pdf)
- [Aragonite](http://rruff.info/doclib/hom/aragonite.pdf)
- [Malaquite](http://rruff.info/doclib/hom/malachite.pdf)

**SULFATOS**
- [Barite](http://rruff.info/doclib/hom/baryte.pdf)
- [Gesso](http://rruff.info/doclib/hom/gypsum.pdf)

**TUNGSTATOS**
- Série das volframites
    
    Exemplo: [ferberite](http://rruff.info/doclib/hom/ferberite.pdf)

**SULFURETOS**
- [Galena](http://rruff.info/doclib/hom/galena.pdf)
- [Esfalerite (blenda)](http://rruff.info/doclib/hom/sphalerite.pdf)
- [Pirite](http://rruff.info/doclib/hom/pyrite.pdf)
- [Calcopirite](http://rruff.info/doclib/hom/chalcopyrite.pdf)
- [Arsenopirite](http://rruff.info/doclib/hom/arsenopyrite.pdf)
- [Pirrotite](http://rruff.info/doclib/hom/pyrrhotite.pdf)
- [Estibina](http://rruff.info/doclib/hom/stibnite.pdf)
- [Molibdenite](http://rruff.info/doclib/hom/molybdenite.pdf)

**HALETOS** (HALOGENETOS/HALOIDES)
- [Halite](http://rruff.info/doclib/hom/halite.pdf)
- [Fluorite](http://rruff.info/doclib/hom/fluorite.pdf)

## minerais da primeira aula

não faço ideia qual foi o tema mas regra geral a risca era escura

- volframite

    - dos Tungstatos só vamos falar da série das volframites, que são tungstatos de Fe. Também têm manganes. $`(Fe,Mn)WO_4`$. Nesta série, podemos ir da ferberite (rica em Fe) até á hubnerite (rica em Mn), mas nós não vamos fazer essa distinção. Ou seja, chamamos a todos eles volframite.
    - a risca é castanho muito escuro/"cholocate preto"
    - tabular e com clivagem lateral 

    ![volframite](img/volframite.png)
    
- grafite
    - elemento nativo, C
    - dureza 1.5, fácil de riscar com a unha
    - tacto untuoso, "desfaz-se todo"
    - clivagem pinacoidal

    ![grafite](img/grafite.png)

- magnetite
    - um óxido (de Fe)
    - tem magnetismo

    ![magnetite](img/magnetite.png)

VS

- hematite
    - um óxido (de Fe)
    - também pode apresentar magnetismo MAS...
    - risca castanha-ferroginosa ("sangue seco")
    - dureza 5.5-6 (*desnecessário*)

    ![hematite](img/hematite.png)


- galena
    - um sulfureto (de chumbo, Pb)
    - dureza 2.5
    - risca cinzenta muito escura, com um brilho intenso
    - forma cubos, e tem clivagem cúbica

    ![galena](img/galena.png)

VS

- esfarelite
    - um sulfureto (de Zn), mas também é o principal minério do Índio (In). O Fe diminui o valor dela
    - dureza 3.5-4
    - é "enganosa/aldrabona" porque pode passar por galena (dureza 2.5).
    - é incolor com risca branca mas á medida que o teor em Fe aumenta, fica mais escura. A cor não se parece muito com a da hematite, mas também se pode reparar que a hematite é mais dura

    ![esfarelite](img/esfarelite.png)


- pirite
    - um sulfureto (de Fe)
    - tonalidade dourada (ouro-dos-tolos)
    - risca bem o vidro (dureza 6-6.8 mas *desnecessário*)

    ![pirite](img/pirite.png)


VS

- calcopirite
    - um sulfureto (de Cu e Fe)
    - tonalidade dourada como a pirite mas não risca o vidro

    ![calcopirite](img/calcopirite.png)

VS

- arsenopirite
    - um sulfureto (de As e Fe), principal minério de arsénio
    - cor é um cinzento pálido, não é tão dourada como a pirite

    ![arsenopirite](img/arsenopirite.png)


VS

- pirrotite
    - sulfureto (de Fe)
    - tonalidade dourada ("cinzento bronze")
    - é magnética

    ![pirrotite](img/pirrotite.png)

    ![pirrotite2](img/pirrotite2.png)

    ![pirrotite3](img/pirrotite3.png)

    um quartzo com intrusão de pirrotite


- estibina
    - sulfureto (de antimónio, 51 Sb)
    - dureza semelhante á da grafite
    - cristais alongados numa única direção, clivagem lateral

    ![estibina](img/estibina.png)





# PL3

**Minerais de risca clara que não riscam o vidro**

(dureza < 5)

- enxofre nativo
    - cor amarela
    - risca também amarela

    ![enxofre](img/enxofre-nativo.jpg)


- No grupo dos silicatos, vimos os filossilicatos. Todos estes ficam dispostos em folhas.

    - micas
        - moscovite
            - silicato de Al e K, com Mg e Fe
            - é a "mica branca". A cor é branca/cinzenta/amarelado
            - clivagem em planos muito finos

            ![moscovite](img/moscovite.jpg)
        
        - biotite
            - é a "mica preta". A cor é castanha/negro
            - "Cuidado com a biotites!". Na biotite "fresca" a risca é branca mas em contacto com a água (e o vapor de água na atmosfera), a risca pode ficar mais escura

            ![biotite](img/biotite.jpg)
        
        - lepidolite
            - em vez do Al, pode ter Si, Rb, Li; principal minério de lítio
            - é a "mica lilás"
            - geralmente não desenvolve cristais grandes. Textura de agregado "sacaróide" (parece açucar)

            ![lepidolite](img/lepidolite.jpg)

            ![lepidolite2](img/lepidolite2.jpg)

    - clorite
        - substitui Mg e Fe por Ni e Mn
        - tonalidade verde (tem tom verde mas a que vi era cinza)
        - separa-se em folhas
        
        ![clorite](img/clorite.jpg)
    
    - talco
        - aparece na escala Mohs
        - menos duro que o gesso (*não confiar*, mas fácil de riscar com a unha)
        - untoso ao tacto

        ![talco](img/talco.jpg)


- No grupo dos fosfatos, só estudamos as apatites
    - apatite
        - fosfato de Ca
        - aparece na escala de Mohs mas "aparece com diferentes cores (é incolor) e parece um bocado baça"

        ![apatite](img/apatite.jpg)

- No grupo dos carbonatos,
    - calcite
        - calcite e aragonite são ambas carbonato de cálcio
        - aparece na escala Mohs

        ![calcite](img/calcite.jpg)

    - aragonite
        - é mais dura que a calcite
        - forma maclas
        - cristais pseudohexagonais

        ![aragonite](img/aragonite.jpg)

- Todos os sulfatos
    - gesso
        - sulfato de Ca
        - aparece na escala Mohs
        - riscado pela unha
        - clivagem pinacoidal

        ![gesso](img/gesso.jpg)
    
    - barite
        - sulfato de bário (Ba)
        - dureza 3-3.5
        - elevada densidade

        ![barite](img/barite.jpg)

- Todos os haletos
    - halite
        - cloreto de sódio (o mineral do sal de cozinha)
        - fica gorduroso com a humidade

        ![halite](img/halite.jpg)
    
    - fluorite
        - fluoreto de Ca
        - aparece na escala Mohs
        - pode ter "cores bonitas" mas não necessariamente
        - dureza 4
        
        ![fluorite](img/fluorite.jpg)
